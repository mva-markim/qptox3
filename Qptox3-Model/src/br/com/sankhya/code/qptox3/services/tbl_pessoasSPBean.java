package br.com.sankhya.code.qptox3.services;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import br.com.sankhya.modelcore.MGEModelException;
import br.com.sankhya.modelcore.util.BaseSPBean;
import br.com.sankhya.ws.ServiceContext;

/**	
 * @author 
 *
 * @ejb.bean name="tbl_pessoasSP"
 *  jndi-name="br/com/sankhya/code/qptox3/model/services/tbl_pessoasSP"
 *  type="Stateless" transaction-type="Container" view-type="remote"
 *
 * @ejb.transaction type="Supports"
 * @ejb.util generate="false"
 */
public class tbl_pessoasSPBean extends BaseSPBean implements SessionBean {

	private static final long serialVersionUID = 1L;
	
	private SessionContext context;

	/**
	 * @ejb.interface-method tview-type="remote"
	 */
	public void metodo(ServiceContext ctx) throws MGEModelException {
		
	}

	/**
	 * @ejb.interface-method tview-type="remote"
	 * @ejb.transaction type="Required"
	 */
	public void newOS(ServiceContext ctx) throws MGEModelException {
		
	}

	

}
