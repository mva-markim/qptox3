angular
    .module('P1App', ['snk'])
    .controller('P1Controller', ['SkApplication', 'i18n', 'ObjectUtils', 'MGEParameters', 'AngularUtil', 'StringUtils', 'ServiceProxy', 'MessageUtils', 'SanPopup',
        function(SkApplication, i18n, ObjectUtils, MGEParameters, AngularUtil, StringUtils, ServiceProxy, MessageUtils, SanPopup) {
            var self = this;

            var _dspone;
            var _dynaformpone;
            var _personalizedFilter;
            var _navigator;

            //Dynaform Interceptors
            self.onDynaformLoaded = onDynaformLoaded;
            self.customTabsLoader = customTabsLoader;
            self.interceptNavigator = interceptNavigator;
            self.interceptPersonalizedFilter = interceptPersonalizedFilter;

            <%=interceptors.interceptorsDynaformHeaders%>            

            //Dataset Interceptors
            ObjectUtils.implements(self, IDataSetObserver);            

            <%=interceptors.interceptorsDatasetHeaders%>

            //Dynaform intercepttors implementation
            ObjectUtils.implements(self, IDynaformInterceptor);
            
            function onDynaformLoaded(dynaform, dataset) {
                _dynaformpone = dynaform;
                _dspone = dataset;

                _dspone.addObserver(self);

                <%=navigatorConfig%>
            }
            
            function customTabsLoader(entityName) {                
                if (entityName == 'pone') {
                    var customTabs = [];
                    //{customTabs placeholder}
                    return customTabs;
                }
            }            

            function interceptPersonalizedFilter(personalizedFilter, dataset) {
                _personalizedFilter = personalizedFilter;
            }

            function interceptNavigator(navigator, dynaform) {
                _navigator = navigator;
            }

            <%=interceptors.interceptorsDynaform%>

            //Dataset interceptors implementation
            <%=interceptors.interceptorsDataset%>
            
            //{popup callers placeholder}           
        }
    ]);